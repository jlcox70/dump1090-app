FROM rockylinux:9 AS builder
RUN sed 's/https/http/g' -i /etc/yum.repos.d/*.repo
RUN dnf install epel-release -y 
RUN    sed 's/https/http/g' -i /etc/yum.repos.d/* 
RUN    dnf install rtl-sdr-devel git make gcc -y 
WORKDIR /
RUN    git clone https://github.com/antirez/dump1090.git 
WORKDIR /dump1090  
RUN    make 
    
FROM rockylinux:9
COPY --from=builder /dump1090 /dump1090    
RUN sed 's/https/http/g' -i /etc/yum.repos.d/* \
    && dnf install epel-release -y \
    && sed 's/https/http/g' -i /etc/yum.repos.d/* \
    && dnf install rtl-sdr -y
EXPOSE 8081 30003
WORKDIR /dump1090    
