TAG := 2
CONTAINER_RUNTIME := $(shell command -v podman || command -v docker)

push: build
	$(CONTAINER_RUNTIME) manifest push docker.io/jlcox1970/dump1090:$(TAG)	

build:
	- $(CONTAINER_RUNTIME) manifest create docker.io/jlcox1970/dump1090:$(TAG)
	- $(CONTAINER_RUNTIME) buildx build --platform linux/amd64,linux/arm64 --manifest docker.io/jlcox1970/dump1090:$(TAG) --security-opt=label=disable .

